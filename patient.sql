create table Patient_Details(Patient int, Name Text, DOB Date, Address Text, Prescription int);
create table Prescription_Details(Prescription int, Drug Text, Date Date, Dosage Text, Doctor Text);
create table Doctor_Details(Doctor Text, Secretary Text);
