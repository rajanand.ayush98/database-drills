create table Doctor_Details(Doctor int, DoctorName Text, Secretary Text);
create table Patient_Doctor(Patient int, Doctor int, Prescription int);
create table Patient_Details(Patient int, PatientName Text, PatientDOB Date, PatientAddress Text);
create table Prescription_Details(Prescription int, Drug Text, Date Date, Dosage Text);