create table Client_Details (Client int, Name Text, Location Text);
create table Manager_Details(Manager int, Manager_name Text, Manager_location Text);
create table Contract_Details(Contract int, Estimated_cost int, Completion_date Date);
create table Staff_Details(Staff int, Staff_name Text, Staff_location Text);
create table Manager_Client(Manager int, Client int);
create table Manager_Staff(Manager int, Staff int);
create table Manager_Contract(Manager int, Contract int);